﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2018 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This version of this source code is freely released under CC0.
	For more details see here: https://creativecommons.org/publicdomain/zero/1.0/
*/

using UnityEngine;

namespace Goblinsama.Anim
{
	public abstract class AnimControllerAbstract : MonoBehaviour
	{
		#region Config
		
		[SerializeField] protected Anim startingAnim;
		
		/// <summary>
		/// L'oggetto a cui cambiare il layer.
		/// </summary>
		[SerializeField] private GameObject targetObject;
		
		/// <summary>
		/// Se cambiare il layer ricorsivamente anche ai figli di targetObject.
		/// </summary>
		[SerializeField] private bool targetRecursive;
		
		/// <summary>
		/// Se true, copia la localPosition dell'Anim nello SpriteRenderer.
		/// Se false, no.
		/// Va usato in caso le Anim abbiano degli offset diversi, mentre si può evitare in caso siano tutte centrate in 0,0.
		/// Se viene usato, accertarsi che lo SpriteRenderer stia in un oggetto a sé, figlio del principale.
		/// Si noti che .mirror funziona solamente se useAnimPosition è true.
		/// </summary>
		[SerializeField] protected bool useAnimPosition;
		
		#endregion
		
		public Anim current { get; protected set; }
		
		// recycle
		
		private Vector3 vec3;
		
		protected virtual void Awake ()
		{
			if ( startingAnim == null )
				startingAnim = GetComponentInChildren<Anim>();
		}
		protected void Start ()
		{
			if ( startingAnim && Application.isPlaying )
			{
				Set( startingAnim );
			}
		}
		protected bool Update ()
		{
			if ( !Application.isPlaying )
				return true;
			
			if ( !current )
				return true;
			
			SetSprite( current.sprite );
			
			return false;
		}
		
		public void Set ( Anim anim, System.Func<bool> callback )
		{
			anim.powerCallback = callback;
			Set( anim );
		}
		public void Set ( Anim anim )
		{
			if ( anim == current )
				return;
			
			if ( current )
				current.gameObject.SetActive( false );
			
			current = anim;
			current.Reset();
			current.gameObject.SetActive( true );
			
			//// lo sprite
			
			SetSprite( current.sprite );
			
			UpdateTransform();
			
			if ( current.sortingLayerName.Length > 0 )
				SetSortingLayer( current.sortingLayerName );
			
			//// il layer
			
			if ( targetObject )
			{
				if ( targetRecursive )
				{
					foreach ( Transform go in targetObject.GetComponentsInChildren<Transform>() )
					{
						go.gameObject.layer = current.gameObject.layer;
					}
				}
				else
				{
					targetObject.layer = current.gameObject.layer;
				}
			}
		}
		
		protected void UpdateTransform ()
		{
			if ( !useAnimPosition )
				return;
			
			vec3 = current.transform.localPosition;
			if ( Mirror() )
				vec3.x = -vec3.x;
			SetSpritePosition( vec3 );
			
			vec3 = current.transform.localScale;
			if ( Mirror() )
				vec3.x = -vec3.x;
			SetSpriteScale( vec3 );
		}
		public virtual bool Mirror ()
		{
			return false;
		}
		
		protected abstract void SetSprite ( Sprite val );
		protected abstract void SetSortingLayer ( string layerName );
		protected abstract void SetSpritePosition ( Vector3 pos );
		protected abstract void SetSpriteScale ( Vector3 scale );
	}
}
