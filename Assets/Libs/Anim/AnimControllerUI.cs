﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This version of this source code is freely released under CC0.
	For more details see here: https://creativecommons.org/publicdomain/zero/1.0/
*/

using UnityEngine;
using UnityEngine.UI;

namespace Goblinsama.Anim
{
	[RequireComponent(typeof(Image))]
	public class AnimControllerUI : AnimControllerAbstract
	{
		private Image imageUI;
		
		protected override void Awake ()
		{
			base.Awake();
			
			imageUI = GetComponent<Image>();
		}
		
		protected override void SetSprite ( Sprite val )
		{
			imageUI.sprite = val;
		}
		protected override void SetSortingLayer ( string layerName )
		{
			;
		}
		protected override void SetSpritePosition ( Vector3 pos )
		{
			imageUI.transform.localPosition = pos;
		}
		protected override void SetSpriteScale ( Vector3 scale )
		{
			imageUI.transform.localScale = scale;
		}
	}
}
