﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This version of this source code is freely released under CC0.
	For more details see here: https://creativecommons.org/publicdomain/zero/1.0/
*/

using UnityEngine;

namespace Goblinsama.Anim
{
	/// <summary>
	/// An Anim for a single direction.
	/// The Anim class can include multiple AnimDirection, and if it does, it displays the correct one,
	/// based on which direction is the character moving.
	/// </summary>
	public class AnimDirection : AnimSprites
	{
		public Vector2 direction;
	}
}
