﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This version of this source code is freely released under CC0.
	For more details see here: https://creativecommons.org/publicdomain/zero/1.0/
*/

using UnityEngine;
using System.Collections.Generic;

namespace Goblinsama.Anim
{
	/// <summary>
	/// The sprites of an animation, be it a full Anim, or a simple AnimDirection.
	/// </summary>
	public class AnimSprites : MonoBehaviour
	{
		[SerializeField] private List<Sprite> sprites;
		
		public List<Sprite> spriteList {
			get {
				return sprites;
			}
			set {
				sprites = value;
			}
		}
	}
}
