﻿using UnityEngine;
using UnityEngine.UI;

public class AnimateButton : MonoBehaviour
{
	private Image image;

	[SerializeField] private Sprite[] frames;
	[SerializeField] private int frameDuration;

	private int last_update;
	private int activeFrame;

	private void Awake ()
	{
		image = GetComponent<Image>();
	}

	private void Update ()
	{
		if ( last_update + frameDuration < Time.frameCount )
		{
			if ( ++activeFrame >= frames.Length )
			{
				activeFrame = 0;
			}
			image.sprite = frames[activeFrame];
			last_update = Time.frameCount;
		}
	}
}
