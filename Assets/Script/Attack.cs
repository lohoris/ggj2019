﻿using UnityEngine;

public class Attack : MonoBehaviour
{
	private void OnTriggerEnter2D ( Collider2D collision )
	{
		if ( collision.tag != "Player" )
			return;

		Corgi player = collision.GetComponent<Corgi>();
		GetComponentInParent<WildDog>().Attack( player );
	}
}
