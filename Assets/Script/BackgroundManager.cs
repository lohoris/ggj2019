﻿using UnityEngine;

public class BackgroundManager : MonoBehaviour
{   
    public GameObject Background1_1;
    public GameObject Background1_2;
    //public GameObject Background2_1;
    //public GameObject Background2_2;
    //public GameObject Background3_1;
    //public GameObject Background3_2;
    public GameObject Player;

    private const float Background1ScrollSpeed = 0.9f;
    private const float Background2ScrollSpeed = 0.8f;
    private const float Background3ScrollSpeed = 0.7f;

    private void Update()
    {
        PositionBackground();
    }

    // NOTE: Shelved due to time constraints.
    //private void ParallaxBackground()
    //{        
    //    float playerX = Player.transform.position.x;
    //    Vector2 newPos;

    //    newPos = Background1_1.transform.position;
    //    newPos.x = playerX * Background1ScrollSpeed;
    //    Background1_1.transform.position = newPos;

    //    newPos = Background2_1.transform.position;
    //    newPos.x = playerX * Background2ScrollSpeed;
    //    Background2_1.transform.position = newPos;

    //    newPos = Background3_1.transform.position;
    //    newPos.x = playerX * Background3ScrollSpeed;
    //    Background3_1.transform.position = newPos;
    //}

    private void PositionBackground()
    {
        float playerX = Player.transform.position.x;
        Bounds playerBounds = Player.GetComponent<BoxCollider2D>().bounds;
        Bounds background1Bounds = Background1_1.GetComponent<BoxCollider2D>().bounds;

        // Check which background is currently used then move backgrounds accordingly.
        bool playerOnBackground1 = playerBounds.Intersects(background1Bounds);

        if (playerOnBackground1)
        {
            float background1X = Background1_1.transform.position.x;
            bool playerOnLeftOfBackground1 = (playerX < background1X);

            if (playerOnLeftOfBackground1)
            {
                MoveExtraBackgroundToLeft(Background1_1, Background1_2);
            }
            else
            {
                MoveExtraBackgroundToRight(Background1_1, Background1_2);
            }
        }
        else
        {
            float background2X = Background1_2.transform.position.x;
            bool playerOnLeftOfBackground1 = (playerX < background2X);

            if (playerOnLeftOfBackground1)
            {
                MoveExtraBackgroundToLeft(Background1_2, Background1_1);
            }
            else
            {
                MoveExtraBackgroundToRight(Background1_2, Background1_1);
            }
        }
    }

    private void MoveExtraBackgroundToLeft(GameObject currentBackground,
        GameObject extraBackground)
    {
        Vector2 newPos = currentBackground.transform.position;
        newPos.x = currentBackground.transform.position.x
            - currentBackground.GetComponent<SpriteRenderer>().bounds.size.x;
        extraBackground.transform.position = newPos;
    }

    private void MoveExtraBackgroundToRight(GameObject currentBackground,
        GameObject extraBackground)
    {
        Vector2 newPos = currentBackground.transform.position;
        newPos.x = currentBackground.transform.position.x
            + currentBackground.GetComponent<SpriteRenderer>().bounds.size.x;
        extraBackground.transform.position = newPos;
    }
}