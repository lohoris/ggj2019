﻿using UnityEngine;

public class Ball : Movable
{
	#pragma warning disable CS0649

	[SerializeField] [Range( 0, 1 )] private float hFriction;

	#pragma warning restore CS0649

	private void FixedUpdate ()
	{
		if ( grounded )
		{
			// We manually decrease the H speed, otherwise the friction will be too low anyway, since the collider is a Circle

			v2 = body.velocity;
			v2.x *= ( 1 - hFriction );
			body.velocity = v2;
		}
	}
}
