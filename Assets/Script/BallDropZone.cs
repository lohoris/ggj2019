﻿using UnityEngine;

public class BallDropZone : MonoBehaviour
{
	private void OnCollisionEnter2D ( Collision2D collision )
	{
		if ( collision.gameObject.CompareTag( "Ball" )
		    || collision.gameObject.CompareTag( "Player" ) )
		{
			FindObjectOfType<Corgi>().Attacked();
		}
	}
}