﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;

    private const float cameraYOffset = 0.7f;
    private const float cameraYLowest = 0;

    private void LateUpdate()
    {
        Vector3 newCameraPos = transform.position;
        newCameraPos.x = player.transform.position.x;
        float newCameraPosY = player.transform.position.y + cameraYOffset;

        // Clamp new camera Y position to lowest.
        newCameraPosY = (newCameraPosY < cameraYLowest ? cameraYLowest : newCameraPosY);
        newCameraPos.y = newCameraPosY;
        transform.position = newCameraPos;
    }
}