﻿using UnityEngine;
using Goblinsama.Anim;

public class Corgi : Movable
{
#pragma warning disable CS0649

	[SerializeField] private float speed;
	[SerializeField] private float jump;
	[SerializeField] private float jumpShort;

	[SerializeField] private BoxCollider2D bitePivot;
	[SerializeField] private BoxCollider2D peePivot;

	[SerializeField] private Anim animIdle;
	[SerializeField] private Anim animWalk;
	[SerializeField] private Anim animJump;
	[SerializeField] private Anim animIdleBall;
	[SerializeField] private Anim animWalkBall;
	[SerializeField] private Anim animJumpBall;
	[SerializeField] private Anim animHappy;
	[SerializeField] private Anim animPee;

#pragma warning restore CS0649

	private Vector3 baseScale;

	public GameObject grabbed { get; private set; }
	private Rigidbody2D grabbedBody;

	private bool disabled;
	private Peeable peeingOn;

	private void Awake ()
	{
		baseScale = transform.localScale;
	}

	private bool Update ()
	{
		if ( disabled )
			return true;

		bool grab = Input.GetButtonDown( "Grab" );
		bool pee = Input.GetButtonDown( "Pee" );

		if ( grab )
		{
			if ( grabbed )
			{
				// Is already holding something, lets it go

				Drop();
			}
			else
			{
				// Mouth empty, grabs a new object

				Collider2D got = Physics2D.OverlapBox( ( Vector2 )bitePivot.transform.position + bitePivot.offset, bitePivot.bounds.extents * 2, 0, movableMask );
				if ( got )
				{
					Grab( got.gameObject );
				}
			}
		}
		if ( pee )
		{
			Collider2D got = Physics2D.OverlapBox( ( Vector2 )peePivot.transform.position + peePivot.offset, peePivot.bounds.extents * 2, 0, peeableMask );
			if ( got )
			{
				Peeable peeable = got.GetComponent<Peeable>();
				if ( peeable )
				{
					Pee( peeable );
					return true;
				}
				else
				{
					Debug.LogWarning( "Couldn't find a peeable object but I should've: " + got );
				}
			}
		}

		// V movement
		{
			// Jump

			bool wantToJump = Input.GetButtonDown( "Jump" );

			if ( wantToJump )
			{
				// Raycast to check if we are touching ground

				if ( grounded )
				{
					// Ok we are touching ground, we can jump
					float jumpCurrent = grabbed ? jumpShort : jump;
					body.AddForce( Vector2.up * jumpCurrent, ForceMode2D.Impulse );
					animController.Set( grabbed ? animJumpBall : animJump );
				}
			}
		}

		if ( grounded && body.velocity.y <= 0 )
		{
			bool walking = Mathf.Abs( body.velocity.x ) > .01f;
			animController.Set( walking ? ( grabbed ? animWalkBall : animWalk ) : ( grabbed ? animIdleBall : animIdle ) );
		}

		return false;
	}

	private void FixedUpdate ()
	{
		if ( disabled )
			return;

		Vector2 vel = body.velocity;

		// H movement
		{
			// H speed

			bool left = Input.GetButton( "Left" );
			bool right = Input.GetButton( "Right" );

			int vx = 0;
			if ( left )
			{
				vx -= 1;
			}
			if ( right )
			{
				vx += 1;
			}

			if ( vx != 0 )
			{
				vel.x = speed * vx;

				// Flips the character

				v3 = baseScale;
				if ( vel.x > 0 )
					v3.x = -v3.x;
				transform.localScale = v3;
			}

			body.velocity = vel;
		}
	}

	protected override bool OnDrawGizmos ()
	{
		if ( base.OnDrawGizmos() )
			return true;

		Gizmos.DrawWireCube( ( Vector2 )bitePivot.transform.position + bitePivot.offset, bitePivot.bounds.extents * 2 );
		Gizmos.DrawWireCube( ( Vector2 )peePivot.transform.position + peePivot.offset, peePivot.bounds.extents * 2 );

		return false;
	}

	private void Grab ( GameObject target )
	{
		grabbed = target;

		grabbedBody = grabbed.GetComponent<Rigidbody2D>();
		if ( grabbedBody )
		{
			grabbedBody.simulated = false;
		}

		grabbed.transform.position = bitePivot.transform.position;
		grabbed.transform.SetParent( bitePivot.transform );
	}
	private void Drop ()
	{
		if ( !grabbed )
			return;

		grabbed.transform.SetParent( null );
		if ( grabbedBody )
		{
			grabbedBody.velocity = Vector2.zero;
			grabbedBody.simulated = true;
		}

		grabbed = null;
		grabbedBody = null;
	}
	private void Pee ( Peeable target )
	{
		//Debug.Log( "peeing on" + target );
		animController.Set( animPee, EndPee );
		peeingOn = target;
		disabled = true;
	}
	
	private bool EndPee ()
	{
		peeingOn.Pee( this );
		disabled = false;
		
		return false;
	}

	public void EndLevel ()
	{
		spriteRenderer.color = Color.yellow;
		Drop();
		animController.Set( animHappy );
		disabled = true;
		FindObjectOfType<UI>().DelayedEndLevel();

		OldMan oldMan = FindObjectOfType<OldMan>();
		if ( oldMan )
		{
			oldMan.EndLevel();
		}
	}
	public void Attacked ()
	{
		spriteRenderer.color = Color.black;
		Drop();
		animController.Set( animIdle );
		disabled = true;
		FindObjectOfType<UI>().GameOver();
	}
}
