﻿using UnityEngine;

public class EndLevel : MonoBehaviour
{
	private void OnTriggerStay2D ( Collider2D collision )
	{
		if ( collision.tag != "Player" )
			return;

		Corgi player = collision.GetComponent<Corgi>();

		if ( player.grabbed )
		{
			// Ok, we can end the level

			player.EndLevel();
		}
	}
}
