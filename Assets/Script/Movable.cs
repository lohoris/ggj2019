﻿using UnityEngine;
using Goblinsama.Anim;

public class Movable : Sprited
{
	private static int _terrainMask;
	public static int terrainMask
	{
		get
		{
			if ( _terrainMask == 0 )
			{
				_terrainMask = 1 << LayerMask.NameToLayer( "Terrain" );
			}
			return _terrainMask;
		}
	}

	private static int _peeableMask;
	public static int peeableMask
	{
		get
		{
			if ( _peeableMask == 0 )
			{
				_peeableMask = 1 << LayerMask.NameToLayer( "Peeable" );
			}
			return _peeableMask;
		}
	}

	private static int _peedMask;
	public static int peedMask
	{
		get
		{
			if ( _peedMask == 0 )
			{
				_peedMask = 1 << LayerMask.NameToLayer( "Peed" );
			}
			return _peedMask;
		}
	}

	private static int _movableMask;
	public static int movableMask
	{
		get
		{
			if ( _movableMask == 0 )
			{
				_movableMask = 1 << LayerMask.NameToLayer( "Movable" );
			}
			return _movableMask;
		}
	}

	private Rigidbody2D _body = null;
	public Rigidbody2D body
	{
		get
		{
			if ( _body == null )
			{
				_body = GetComponent<Rigidbody2D>();
			}
			return _body;
		}
	}

	private Collider2D _coll = null;
	protected Collider2D coll
	{
		get
		{
			if ( _coll == null )
			{
				_coll = GetComponent<Collider2D>();
			}
			return _coll;
		}
	}

	private AnimController _animController = null;
	protected AnimController animController
	{
		get
		{
			if ( _animController == null )
			{
				_animController = GetComponent<AnimController>();
			}
			return _animController;
		}
	}

	private bool _grounded;
	private int groundedLastCheck;
	protected bool grounded
	{
		get
		{
			if ( Time.frameCount != groundedLastCheck )
			{
				_grounded = GroundCheckOnX( coll.bounds.center.x )
					|| GroundCheckOnX( coll.bounds.min.x )
					|| GroundCheckOnX( coll.bounds.max.x );

				groundedLastCheck = Time.frameCount;
			}
			return _grounded;
		}
	}

	#pragma warning disable CS0649

	[SerializeField] protected float raycastLength = .04f;

	#pragma warning restore CS0649

	// Optimisation
	protected Vector2 v2;
	protected Vector3 v3;

	protected virtual bool OnDrawGizmos ()
	{
		if ( !coll )
			return true;

		Gizmos.color = Color.black;

		v2.Set( coll.bounds.center.x, coll.bounds.min.y );
		Gizmos.DrawLine( v2, v2 + Vector2.down * raycastLength );

		v2.Set( coll.bounds.max.x, coll.bounds.min.y );
		Gizmos.DrawLine( v2, v2 + Vector2.down * raycastLength );

		v2.Set( coll.bounds.min.x, coll.bounds.min.y );
		Gizmos.DrawLine( v2, v2 + Vector2.down * raycastLength );

		return false;
	}

	private bool GroundCheckOnX ( float xx )
	{
		v2.Set( xx, coll.bounds.min.y );
		RaycastHit2D hit = Physics2D.Raycast( v2, Vector2.down, raycastLength, terrainMask );
		return ( bool )hit;
	}
}
