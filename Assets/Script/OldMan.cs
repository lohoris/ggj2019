﻿using UnityEngine;
using Goblinsama.Anim;

public class OldMan : Movable
{
	[SerializeField] [Range( 1, 4 )] private float throwDelayMin;
	[SerializeField] [Range( 2, 16 )] private float throwDelayMax;

	[SerializeField] private Ball ball;

	[SerializeField] private Transform throwDirection;
	[SerializeField] private float throwStrength;

	[SerializeField] private Anim animIdle;
	[SerializeField] private Anim animThrow1;
	[SerializeField] private Anim animThrow2;
	[SerializeField] private Anim animPet;

	private float throwAt;
	private bool thrown;
	private Vector2 forceVector;

	private void Start ()
	{
		thrown = false;
		if ( ball )
		{
			float throwDelay = Random.Range( throwDelayMin, throwDelayMax );
			throwAt = Time.time + throwDelay;
			forceVector = ( throwDirection.position - animThrow2.transform.position ).normalized * throwStrength;
			//Debug.Log( "will throw at " + throwAt );
		}
	}

	private void Update ()
	{
		if ( thrown || !ball )
			return;

		if ( Time.time > throwAt )
		{
			//Debug.Log( "throwing!" );
			thrown = true;

			//Debug.Log( "ball layer is "+ ball.gameObject.layer );
			ball.body.simulated = false;
			ball.transform.position = animThrow1.transform.position;
			ball.gameObject.SetActive( true );
			//Debug.Log( "ball layer is " + ball.gameObject.layer );

			animController.Set( animThrow1, () =>
			{
				ball.transform.position = animThrow2.transform.position;

				animController.Set( animThrow2, () =>
				{
					ball.body.simulated = true;
					ball.body.AddForce( forceVector, ForceMode2D.Impulse );

					animController.Set( animIdle );

					return false;
				} );

				return false;
			} );
		}
	}

	public void EndLevel ()
	{
		animController.Set( animPet );
	}
}
