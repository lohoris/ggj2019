﻿using UnityEngine;

public class SoundEffects : MonoBehaviour
{
    public void PlayAudioSource()
    {
        GetComponent<AudioSource>().Play();
    }
}