﻿using UnityEngine;

public class Sprited : MonoBehaviour
{
	private SpriteRenderer _spriteRenderer = null;
	protected SpriteRenderer spriteRenderer
	{
		get
		{
			if ( _spriteRenderer == null )
			{
				_spriteRenderer = GetComponent<SpriteRenderer>();
			}
			return _spriteRenderer;
		}
	}
}
