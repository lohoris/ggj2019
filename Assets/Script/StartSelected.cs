﻿using UnityEngine;
using UnityEngine.UI;

public class StartSelected : MonoBehaviour
{
	private void Start ()
	{
		GetComponent<Selectable>().Select();
	}
}
