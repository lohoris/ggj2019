﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
	private static int currentLevel = 0;

	[SerializeField] private GameObject menuButton;
	[SerializeField] private GameObject options;
	[SerializeField] private GameObject gameOver;

	public void StartGame ()
	{
		SetLevel( 1 );
	}
	public void BackToTitle ()
	{
		Reset();
	}
	public void EndLevelI ()
	{
		EndLevel();
	}
	public void DelayedEndLevel ()
	{
		Invoke( "EndLevelI", 2 );
	}
	public void RestartLevel ()
	{
		LoadCurrentLevel();
		InGameMenus();
	}
	public void OpenMenu ()
	{
		options.SetActive( true );
		TimeScaleUpdate();
	}
	public void CloseMenu ()
	{
		options.SetActive( false );
		TimeScaleUpdate();
	}
	public void ToggleMenu ()
	{
		options.SetActive( !options.activeSelf );
		TimeScaleUpdate();
	}
	public void GameOver ()
	{
		menuButton.SetActive( false );
		options.SetActive( false );
		gameOver.SetActive( true );
		// NOTE: it's intentional we don't modify the timeScale here
	}

	private void TimeScaleUpdate ()
	{
		Time.timeScale = options.activeSelf ? 0 : 1;
	}
	private void InGameMenus ()
	{
		menuButton.SetActive( true );
		options.SetActive( false );
		gameOver.SetActive( false );
		TimeScaleUpdate();
	}

	private static void SetLevel ( int level )
	{
		currentLevel = level;
		LoadCurrentLevel();
	}
	private static void LoadCurrentLevel ()
	{
		SceneManager.LoadScene( currentLevel, LoadSceneMode.Single );
		Time.timeScale = 1;
	}

	public static void Reset ()
	{
		SetLevel( 0 );
	}
	public static void EndLevel ()
	{
		SetLevel( currentLevel + 1 );
	}
}
