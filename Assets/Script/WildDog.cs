﻿using UnityEngine;
using Goblinsama.Anim;

public class WildDog : Movable
{
	[SerializeField] private float PatrolDistance = 1;
	[SerializeField] private float patrolDistancePee = .5f;
	[SerializeField] private float PatrolSpeed = 0.02f;

	[SerializeField] private Vector2 peeDetectorOffset;
	[SerializeField] private float peeDetectorRange = 10f;
	
	[SerializeField] private Anim animIdle;
	[SerializeField] private Anim animWalk;
	[SerializeField] private Anim animAttack;
	[SerializeField] private Anim animPee;

	private float startingPosX;
	private int patrolDir;
	private GameObject peedFound;

	private void Start ()
	{
		startingPosX = transform.position.x;
		patrolDir = 1;
	}

	private void Update ()
	{
		if ( Time.timeScale <= 0 )
			return;
		
		SeekPee();
		
		Patrol();
	}

	protected override bool OnDrawGizmos ()
	{
		if ( base.OnDrawGizmos() )
			return true;

		int pdx = patrolDir != 0 ? patrolDir : -1;
		Vector2 from = ( Vector2 )transform.position + peeDetectorOffset;
		Gizmos.DrawLine( from, from + Vector2.right * pdx * peeDetectorRange );

		return false;
	}

	private bool SeekPee ()
	{
		if ( patrolDir == 0 )
			return false;

		Vector2 from = ( Vector2 )transform.position + peeDetectorOffset;
		RaycastHit2D hit = Physics2D.Raycast( from, Vector2.right * patrolDir, peeDetectorRange, peedMask );
		if ( hit )
		{
			peedFound = hit.collider.gameObject;

			startingPosX = peedFound.transform.position.x;

			return true;
		}
		return false;
	}

	private void Patrol ()
	{
		if ( patrolDir == 0 )
			return;

		animController.Set( animWalk );

		// If patrol distance reached, change direction.
		float currentPosX = transform.position.x;

		bool changeDir = ( ( currentPosX - startingPosX ) * patrolDir ) >= ( peedFound ? patrolDistancePee : PatrolDistance );

		if ( changeDir )
		{
			patrolDir *= -1;
		}

		// Move.
		Vector2 translate = new Vector2( PatrolSpeed * patrolDir, 0 );
		transform.Translate( translate );

		// Point sprite in correct direction.
		Vector3 newScale = transform.localScale;
		newScale.x = -patrolDir;
		transform.localScale = newScale;
	}

	public void Attack ( Corgi target )
	{
		animController.Set( animAttack, EndAttack );
		patrolDir = 0;
		target.Attacked();
	}
	private bool EndAttack ()
	{
		animController.Set( animIdle );
		return false;
	}
}